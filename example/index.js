import "./styles.css";
import TreeList from "../src/app.js";
import Backbone from "backbone";

let treeList = new TreeList;

document.addEventListener("DOMContentLoaded", function () {
    Backbone.history.start();
    // рендер
    treeList.render();

    // найти элемент по айди и сделать его активным
    treeList.activeById('topicId704044');
}, false);

module.exports = treeList;