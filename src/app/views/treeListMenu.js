import $ from 'jquery';
import TreeListUl from './treeListUl';

let TreeListMenu = Backbone.View.extend({

    className: 'tree-list__menu',

    render: function(){
        let treeListUl = new TreeListUl({
            collection: this.collection
        });

        this.$el.html(treeListUl.render().el);
        return this;
    }
});

export default TreeListMenu;