import $ from 'jquery';
import TreeListUl from './treeListUl';
import TreeListCollection from '../collections/treeListCollection';
import Vent from '../vent';

let TreeListItem = Backbone.View.extend({

    tagName: 'li',

    className: 'tree-list__item',

    attributes: {},

    // шаблон элемента списка, 
    // включает в себя ссылку на страницу и элемент для сворачивания дочернего списка
    template: _.template(`
        <a class="tree-list__link" href="<%= url %>"><%= title %></a>
        <% if (typeof pages !== 'undefined' && pages) { %><span class="tree-list__arrow"></span><% } %>
    `),

    // инициализация
    initialize: function(){
        this.attributes['data-uid'] = this.model.get('id');

        this.model.on('change:_active', this.toggleActiveState, this);
        this.model.on('change:_hidden', this.toggleHiddenState, this);
    },

    // события
    events: {
        'click .tree-list__link' : 'linkClick',
        'click .tree-list__arrow': 'arrowClick'
    },

    // переключить класс скрытого стиля
    toggleHiddenState: function(){
        let hiddenClass = 'tree-list__item_hidden';
        
        this.model.get('_hidden') 
            ?   this.$el.addClass(hiddenClass)
            :   this.$el.removeClass(hiddenClass);
    },

    // переключить состояние активной ссылки
    toggleActiveState: function(){
        let activeClass = 'tree-list__item_active';

        // сбрасываем активыне элементы из всех коллекций
        Vent.trigger('activeModel:reset');

        if (this.model.get('_active')) {
            // установка активной модели в коллекцию
            this.model.collection._active = this.model;

            // открыть все древовидные элементы
            this.openNestedLi();

            // установка css класса
            this.$el.addClass(activeClass);
        } else {
            // удаление css класса
            this.$el.removeClass(activeClass);
        } 
    },

    // открыть все древовидные элементы по узлу
    openNestedLi: function(){
        let openedClass = 'tree-list__item_opened';

        this.$el.parents('li').addClass(openedClass);

        if (this.model.get('pages')) {
            this.$el.addClass(openedClass);
        }
    },

    // событие при клике на стрелку
    arrowClick: function(e){
        e = e || window.event;
        e.stopPropagation();

        let openedClass = 'tree-list__item_opened';

        this.$el.toggleClass(openedClass);
    },

    // событие при клике на ссылку
    linkClick: function(e){
        e = e || window.event;
        e.preventDefault();
        e.stopPropagation();

        let $tripList = $('#tree-list');

        // устанавливаем активен ли класс в модель
        this.model.set('_active', true);
    },

    // отрисовка вложенного списка
    renderNestedList: function(){
        // если нету вложенных элементов для меню
        if (!this.model.get('pages')) return;

        // новая коллекция дочернего меню
        let collection = new TreeListCollection;
        collection.set(this.model.get('pages'));

        // вид дочернего меню
        let childrenUl = new TreeListUl({
            collection: collection
        });

        // прикрепляем вид дочернего меню к текущему элементу
        this.$el.append(childrenUl.render().el);
    },

    // отрисовка
    render: function(){
        // шаблон элемента списка
        let template = this.template(this.model.toJSON());
        this.$el.html(template);
        
        // рендер вложеных списков
        this.renderNestedList();

        return this;
    }
});

export default TreeListItem;