import TreeListItem from './treeListItem';

let TreeListUl = Backbone.View.extend({
    
    tagName: 'ul',
    
    className: 'tree-list__list',
    
    // рендер каждого отдельного элемента списка
    addOne: function(model){
        let viewItem = new TreeListItem({
            model: model
        });
        this.$el.append(viewItem.render().el);
    },

    // рендер
    render: function(){
        this.collection.each(
            (model) => {this.addOne(model)}
        );
        return this;
    }
})

export default TreeListUl;