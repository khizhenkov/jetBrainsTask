import TreeListSearch from './treeListSearch';
import TreeListMenu from './treeListMenu';
import TreeListCollection from '../collections/treeListCollection';
import TreeListModel from '../models/treeListModel';
import Vent from '../vent';

let TreeListView = Backbone.View.extend({
    // прикрепляемся к существующему элементу
    el: '#tree-list',

    // модель вида
    model: new TreeListModel,

    // инициализация
    initialize: function(){
        // добавление класса поиска к родительскому элементу
        let addSearchClass = function(){
            this.$el.addClass('tree-list_search');
        }

        // удаление класса поиска с родительского элемента
        let removeSearchClass = function(){
            this.$el.removeClass('tree-list_search');
        }

        Vent.on('search:start', addSearchClass, this);
        Vent.on('search:end', removeSearchClass, this);
    },

    // публичный метод
    // для активации элемента по айди
    activeById: function(id){
        let self = this;
        // добавить колбек функцию
        let addFunction = function(){
            let fn = arguments[0];

            if (typeof fn !== 'function') return;

            // если отрисовка произошла
            if (self.model.get('_rendered')) {
                fn.call(null);
                return;
            }

            let callbacks = self.model.get('callbacks');
            callbacks.push(fn);
            self.model.set('callbacks', callbacks);
        }

        // функция обертка
        let callBackFunction = () => 
            Vent.trigger('searchById:start', _.escape(id));
        
        addFunction(callBackFunction);
    },

    // рендер вида
    render: function(){
        let self = this;

        // очищаем предыдущие колбеки
        this.model.clearCallbacks();

        // добавить спиннер загрузки
        let addSpinner = function(){
            self.$el.html('<div class="tree-list__spinner"></div>');
        }

        // удалить спиннер загрузки
        let removeSpinner = function(){
            self.$el.find('.tree-list__spinner').remove();
        }

        // вызов колбек функции после рендера   
        let executeCallback = function(){
            // коллекция колбэк функции
            let callbacks = self.model.get('callbacks');

            if (!_.size(callbacks)) return;
            _.each(callbacks, (fn) => {fn.call(null)});
        }

        // отрисовка блока с поиском
        let drawSearchBlock = function(){
            let searchBlock = new TreeListSearch;
            self.$el.append(searchBlock.render().el);
        }

        // отрисовка блока с меню
        let drawMenuBlock = function(){
            let menuBlock = new TreeListMenu({
                collection: self.collection
            });
            
            self.$el.append(menuBlock.render().el);
        }

        // коллекция меню
        this.collection = new TreeListCollection;

        // добавить спиннер
        addSpinner();

        // получаем JSON с меню, затем отрисовываем
        this.collection.getInitialJSONData().then(() => {
            // отрисовка поиска
            drawSearchBlock();
            
            // отрисовка меню
            drawMenuBlock();

            // удаление спиннера
            removeSpinner();

            // вызов колбекфункции
            executeCallback();

            // установка в модель значения, что вид отрендерился
            this.model.set('_rendered', true);
        });
    }
});

export default TreeListView;