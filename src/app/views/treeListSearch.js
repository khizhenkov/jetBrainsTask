import Vent from '../vent';
import TreeListSearchModel from '../models/treeListSearch';

let TreeListSearch = Backbone.View.extend({
    tagName: 'label',

    className: 'tree-list__search',

    // необходимый аттрибут для лейбла
    attributes: {
        "for": "tree-list__search"
    },
    
    // модель поиска
    model: new TreeListSearchModel,

    // шаблон с инпутом для поиска
    template: _.template(`
        <input type="text" id="tree-list__search" class="tree-list__search-input" value="<% if (typeof value !== 'undefined' && value) { %><%= value %><% } %>" placeholder="Search IntelliJ IDEA help">
        <div class="tree-list__clear js-clear"></div>
    `),
    
    events: {
        'keyup':                'keyUp',
        'click .js-clear':   'clearInput'
    },

    initialize: function(){
        _.bindAll(this, 'search');

        // метод отпускания клавиши, вызываемый через debounce
        this._keyUp = _.debounce(
            _.bind(this.setValue, this), 300
        );

        this.model.on('change', this.search, this);
    },

    // установка значения
    setValue: function(){
        let val = arguments[0];
        this.model.set('value', val);

        if (val) {
            this.showClearBlock();
        } else {
            this.hideClearBlock();
        }
    },

    // очистка инпута
    clearInput: function(){
        this.$el.find('input').val('');
        this.setValue(null);
    },

    // удалить блок очистки
    hideClearBlock: function(){
        this.$el.find('.js-clear').hide();
    },

    // добавить блок очистки
    showClearBlock: function(){
        this.$el.find('.js-clear').show();
    },

    // триггер поиска
    search: function(){
        let value = this.model.get('value');

        // если значение не пустое, то запускаем поиск
        if (value && value.length) {
            Vent.trigger('search:start', value);
            return;
        }

        Vent.trigger('search:end', value);
    },

    // событие при отпускании клавиши
    keyUp: function(e){
        e = e || window.event;
        this._keyUp(e.target.value);
    },

    // рендер
    render: function(){
        this.$el.html(this.template(this.model.toJSON()));
        return this;
    }
});

export default TreeListSearch;