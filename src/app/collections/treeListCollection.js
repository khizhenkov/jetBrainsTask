import TreeItemModel from '../models/treeItem';
import Vent from '../vent';

let TreeListCollection = Backbone.Collection.extend({
    url: '/HelpTOC.json',

    // используем модель для коллекции
    model: TreeItemModel,

    // инициализация
    initialize: function(){
        Vent.on('activeModel:reset', this.resetActiveModel, this);
        Vent.on('search:start', this.findResults, this);
        Vent.on('search:end', this.resetStates, this);
        Vent.on('searchById:start', this.findModelById, this);
    },

    // сбросить состояние фильтрации
    resetStates: function(){
        this.each((item) => {
            item.set('_hidden', false);
        });
    },

    // найти текст в моделях
    findResults: function(){
        let text = arguments[0];
        let reg = new RegExp(text, 'mig');

        this.each((item) => {
            let result = item.get('title').match(reg);
            item.set('_hidden', !result);
        });
    },

    // найти модель по айди и сделать ее активной
    findModelById: function(){
        let id = arguments[0];
        var model = this.get(id);

        // если модель найдена, то применить состояние активности
        if (model) {
            model.set('_active', true);
        }
    },

    // сброс активной модели в коллекции
    resetActiveModel: function(){
        // если нету активной модели в коллекции
        if (!this._active) return;

        // устанавить, что данная модель более не активна 
        this._active.set('_active', false);

        // удалить данную активную модель из коллекции
        delete this._active;
    },

    // получить дерево элементов
    getInitialJSONData: function(){
        return this.fetch();
    }
});

export default TreeListCollection;