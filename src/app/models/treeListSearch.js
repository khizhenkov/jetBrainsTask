let TreeListSearchModel = Backbone.Model.extend({
    defaults:{
        value: null
    }
});

export default TreeListSearchModel;