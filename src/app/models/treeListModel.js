let TreeListModel = Backbone.Model.extend({
    defaults: {
        callbacks: []
    },
    clearCallbacks: function(){
        this.set('callbacks', [])
    }
});

export default TreeListModel;