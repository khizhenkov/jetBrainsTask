import TreeListView from '../views/treeList';

let AppRouter = Backbone.Router.extend({
    routes:{
        '*other' : 'defaultRouter'
    },
    defaultRouter: function(){
        var view = new TreeListView;
        view.render();
    }
});

export default AppRouter;