const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require("html-webpack-plugin");

const PATHS = {
  example: path.join(__dirname, "example")
};

module.exports = {
    entry: {
        "examples": path.resolve(PATHS.example, "index")
    },
    devtool: 'source-map',
    output: {
        filename: './dist/bundle.js',
        library: 'TreeList'
    },
    devServer: {
        contentBase: PATHS.example
    },
    resolve: {
        alias: {
            'underscore': 'lodash',
            '$': 'jquery'
        }
    },
    plugins: [
        new webpack.ProvidePlugin({
            $: "jquery",
            Backbone: "backbone",
            _: "lodash"
        })
    ],
    module: {
        loaders: [{
            test:   /\.js$/,
            loader: ['babel-loader'],
            exclude: '/node_modules/'
        },{
            test:   /\.css$/,
            loader: ['style-loader', 'css-loader'],
            exclude: '/node_modules/'
        }]
    }
}